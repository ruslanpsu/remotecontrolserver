﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RemoteControlServer
{
    class HttpServer
    {

        public ILogger Logger;
        private HttpListener listener;
        private int listenerPort;
        private Thread listenerThread;

        private CommandExecutor commandExecutor = new CommandExecutor();
                      
        public HttpServer(int port)
        {
        
            listenerPort = port;                 

            try
            {
                listener = new HttpListener();
                listener.IgnoreWriteExceptions = true;



            }
            catch (PlatformNotSupportedException)
            {
                listener = null;
            }
        }

        public bool PlatformNotSupported
        {
            get
            {
                return listener == null;
            }
        }

        public Boolean StartHTTPListener()
        {
            if (PlatformNotSupported)
                return false;

            try
            {
                if (listener.IsListening)
                    return true;

                string prefix = "http://+:" + listenerPort + "/";
                listener.Prefixes.Clear();
                listener.Prefixes.Add(prefix);
                listener.Start();

                if (listenerThread == null)
                {
                    listenerThread = new Thread(HandleRequests);
                    listenerThread.Start();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Boolean StopHTTPListener()
        {
            if (PlatformNotSupported)
                return false;

            try
            {
                listenerThread.Abort();
                listener.Stop();
                listenerThread = null;
            }
            catch (HttpListenerException)
            {
            }
            catch (ThreadAbortException)
            {
            }
            catch (NullReferenceException)
            {
            }
            catch (Exception)
            {
            }
            return true;
        }

        private void HandleRequests()
        {

            while (listener.IsListening)
            {
                var context = listener.BeginGetContext(new AsyncCallback(ListenerCallback), listener);
                context.AsyncWaitHandle.WaitOne();
            }
        }

        private bool RequestIsAjax(HttpListenerRequest request)
        {
            return request.Headers.Get("X-Requested-With") == "XMLHttpRequest";
        }

        private void WriteTextToResponse(HttpListenerResponse response, string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            //context.Response.AddHeader("Cache-Control", "no-cache");
            response.ContentLength64 = buffer.Length;
            response.ContentType = "text/html";

            try
            {
                Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                output.Close();
            }
            catch (HttpListenerException)
            {
            }

            response.Close();
        }

        private void ListenerCallback(IAsyncResult result)
        {

            HttpListener listener = (HttpListener)result.AsyncState;
            if (listener == null || !listener.IsListening)
                return;

            // Call EndGetContext to complete the asynchronous operation.
            HttpListenerContext context;
            try
            {
                context = listener.EndGetContext(result);
            }
            catch (Exception)
            {
                return;
            }

            HttpListenerRequest request = context.Request;

            if (RequestIsAjax(request))
            {
                string command = request.RawUrl.Substring(1);
                if (Logger != null)
                {
                    Logger.Log(command);
                }
                //  выполнение команды с параметрами
                
                command = command.Split('?')[0];
                bool isSuccess = commandExecutor.Execute(command, request.QueryString);
                if (isSuccess)
                {
                    WriteTextToResponse(context.Response, "OK");
                }
                else
                {
                    Logger.Log("Неизвестная команда: " + command);
                    WriteTextToResponse(context.Response, "Error");
                };
            }
            else
            {
                string requestedFile = request.RawUrl.Substring(1);

                if (string.IsNullOrEmpty(requestedFile))
                {
                    requestedFile = @"web\main.html";
                    
                };
                if (File.Exists(requestedFile))
                {
                    //  отдача файла
                    ReturnFile(context, requestedFile);
                    return;
                }
            }                              
               
        }          

           
        private static void ReturnFile(HttpListenerContext context, string filePath)
        {
            context.Response.ContentType = GetcontentType(Path.GetExtension(filePath));
            const int bufferSize = 1024 * 512; //512KB
            var buffer = new byte[bufferSize];
            using (var fs = File.OpenRead(filePath))
            {

                context.Response.ContentLength64 = fs.Length;
                int read;
                while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                    context.Response.OutputStream.Write(buffer, 0, read);
            }

            context.Response.OutputStream.Close();
        }

        private static string GetcontentType(string extension)
        {
            switch (extension)
            {
                case ".avi": return "video/x-msvideo";
                case ".css": return "text/css";
                case ".doc": return "application/msword";
                case ".gif": return "image/gif";
                case ".htm":
                case ".html": return "text/html";
                case ".jpg":
                case ".jpeg": return "image/jpeg";
                case ".js": return "application/x-javascript";
                case ".mp3": return "audio/mpeg";
                case ".png": return "image/png";
                case ".pdf": return "application/pdf";
                case ".ppt": return "application/vnd.ms-powerpoint";
                case ".zip": return "application/zip";
                case ".txt": return "text/plain";
                default: return "application/octet-stream";
            }
        }                     

        public int ListenerPort
        {
            get { return listenerPort; }
            set { listenerPort = value; }
        }

        ~HttpServer()
        {
            if (PlatformNotSupported)
                return;

            StopHTTPListener();
            listener.Abort();
        }

        public void Quit()
        {
            if (PlatformNotSupported)
                return;

            StopHTTPListener();
            listener.Abort();
        }
    }
    
}
