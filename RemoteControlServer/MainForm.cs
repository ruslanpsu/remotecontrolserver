﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using RemoteControlServer;

namespace RemoteControlServer
{
    public partial class MainForm : Form
    {
        delegate void LogCallback(string msg);
        HttpServer httpServer;
        public MainForm()
        {
            InitializeComponent();
            httpServer = new HttpServer(777);
            httpServer.Logger = new Logger(this);
            httpServer.StartHTTPListener();
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            httpServer.Quit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            MouseOperations.MousePoint point = MouseOperations.GetCursorPosition();
            label1.Text = string.Format("{0}:{1}", point.X, point.Y);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MouseOperations.MousePoint point = MouseOperations.GetCursorPosition();
            point.X += 10;
            point.Y += 10;
            MouseOperations.SetCursorPosition(point);
        }

        public void Log(string msg)
        {
            if (rtbLog.InvokeRequired)
            {
                LogCallback d = new LogCallback(Log);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                rtbLog.AppendText(msg + Environment.NewLine);
            }
        }

    }
}
