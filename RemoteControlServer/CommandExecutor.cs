﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlServer
{
    class CommandExecutor
    {        
        public bool Execute(string command, NameValueCollection param)
        {
            switch (command)
            {               

                case "mouse_LeftClick":

                    MouseOperations.LeftButtonClick();
                    break;

                case "mouse_RightClick":

                    MouseOperations.RightButtonClick();
                    break;

                case "mouse_SetCursorOffset":

                    int dx = Convert.ToInt32(Math.Round(double.Parse(param.Get("x"), CultureInfo.InvariantCulture)));
                    int dy = Convert.ToInt32(Math.Round(double.Parse(param.Get("y"), CultureInfo.InvariantCulture)));

                    MouseOperations.setCursorOffset(dx, dy);
                    
                    break;

                case "mouse_DoubleClick":

                    MouseOperations.DoubleClick();
                    break;

                case "mouse_WheelRotateUp":

                    MouseOperations.rotateMouseWheel(120);
                    break;

                case "mouse_WheelRotateDown":

                    MouseOperations.rotateMouseWheel(-120);
                    break;
                case "mouse_WheelClick":

                    MouseOperations.WheelClick();
                    break;

                default:
                    return false; 
            }
            
            return true;
        }       

    }
}
