﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlServer
{
    public class MouseOperations
    {
        const int step = 10;

        [Flags]
        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010,
            Wheel = 0x00000800            
        }

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int X, int Y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out MousePoint lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public static void SetCursorPosition(int X, int Y)
        {
            SetCursorPos(X, Y);
        }

        public static void SetCursorPosition(MousePoint point)
        {
            SetCursorPos(point.X, point.Y);
        }

        public static MousePoint GetCursorPosition()
        {
            MousePoint currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
            return currentMousePoint;
        }

        public static void MouseEvent(MouseEventFlags value, int dwData)
        {
            MousePoint position = GetCursorPosition();

            mouse_event
                ((int)value,
                 position.X,
                 position.Y,
                 dwData,
                 0);
        }

        public static void MouseEvent(MouseEventFlags value)
        {
            MouseEvent(value, 0);
        }

       public static void LeftButtonClick()
       {
            MouseEvent(MouseEventFlags.LeftDown | MouseEventFlags.LeftUp);
        }

        public static void RightButtonClick()
        {
            MouseEvent(MouseEventFlags.RightDown | MouseEventFlags.RightUp);
        }

        public static void DoubleClick()
        {
            LeftButtonClick();
            LeftButtonClick();
        }

        public static void setCursorOffset(int dx, int dy)
        {
            MousePoint point = GetCursorPosition();
            point.X += dx;
            point.Y += dy;
            SetCursorPosition(point);
        }

        public static void rotateMouseWheel(int amount)
        {
            MouseEvent(MouseEventFlags.Wheel, amount);
        }

        public static void WheelClick()
        {
            MouseEvent(MouseEventFlags.MiddleDown|MouseEventFlags.MiddleUp);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePoint
        {
            public int X;
            public int Y;

            public MousePoint(int x, int y)
            {
                X = x;
                Y = y;
            }
        }
    }
}
