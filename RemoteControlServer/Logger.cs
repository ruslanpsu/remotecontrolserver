﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteControlServer
{
    class Logger: ILogger
    {
        private MainForm _mainForm;
        public Logger(MainForm mainForm)
        {
            _mainForm = mainForm;
        }
        public void Log(string msg)
        {
            _mainForm.Log(msg);
        }
    }
}
