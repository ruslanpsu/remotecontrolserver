(function(){
    touchArea = document.getElementById('touch_area');
    touchArea.addEventListener('touchstart', handleTouchStart, false);        
    touchArea.addEventListener('touchmove', handleTouchMove, false);
    touchArea.addEventListener('touchend', handleTouchEnd, false);

    var xDown = null;                                                        
    var yDown = null;                                                        
    var count = 0;    
    var startPoint = {};
    var prevPoint = {x:0, y:0};
    var DISTANCE_THRESHOLD = 10;
    var drSum = 0;
    var prevFixedPoint = {x:0, y:0};

    function sendCursorOffset(offset){
        $.get({
            url:"mouse_SetCursorOffset", 
            data: offset,
            success: function(data){

            }
        });
    }

    function sendMouseLeftClick(){
        $.get({
            url:"mouse_LeftClick", 
           
        });
    }

    function sendMouseDoubleClick(){
        $.get({
            url:"mouse_DoubleClick", 
           
        });
    }

    function PointEquals(p1, p2) {  
        return p1.x == p2.x && p1.y == p2.y;
    }
    
    function handleTouchStart(evt) {              
        console.log("handleTouchStart");                         
        xDown = evt.touches[0].clientX;                                      
        yDown = evt.touches[0].clientY;   

        startPoint.x = xDown;
        startPoint.y = yDown;        

        prevPoint = startPoint;

        prevFixedPoint = startPoint;

        
        console.log("tapped at: ("+evt.touches[0].clientX+"," + evt.touches[0].clientY +")");                                   
    };                                                

    function getDistance(p1, p2){
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    function PointDifference(p1, p2){
        var result = {};
        result.x = p1.x - p2.x;
        result.y = p1.y - p2.y;
        return result;
    }

    var tapped=false;
    function handleTouchEnd(evt){
        count = 0;
        console.log("handleTouchEnd");
        //return;
        point = {};
        point.x = evt.changedTouches[0].clientX;                                      
        point.y = evt.changedTouches[0].clientY; 

        if(!tapped){ //if tap is not set, set up single tap
        tapped=setTimeout(function(){
            tapped=null
            //insert things you want to do when single tapped   
            if (PointEquals(startPoint, point)) {
                sendMouseLeftClick();
            };       
            
            },300);   //wait 300ms then run single click code
        } else {    //tapped within 300ms of last tap. double tap
            clearTimeout(tapped); //stop single tap callback
            tapped=null
            //insert things you want to do when double tapped
            sendMouseDoubleClick();
        };
    }

    function handleTouchMove(evt) {
        console.log("handleTouchMove");
        evt.preventDefault();   //  чтобы при свайпе вниз не обновлялась страница
        var ms = new Date().getTime();

        var point = {};
        point.x = evt.touches[0].clientX;
        point.y = evt.touches[0].clientY;

        var d = getDistance(startPoint, point) ;

        var difPoint = PointDifference(point, prevPoint);
        var dr = getDistance(point, prevPoint);

        drSum += dr;
        if (drSum >= DISTANCE_THRESHOLD){            
            drSum = 0;
            var offset = PointDifference(point, prevFixedPoint);
            /*offset.x *= 1.2;
            offset.y *= 1.2;
            */
            sendCursorOffset(offset);
            prevFixedPoint = point;
        };
        

        prevPoint = point;

        if ( ! xDown || ! yDown ) {
            return;
        }

        var xUp = evt.touches[0].clientX;                                    
        var yUp = evt.touches[0].clientY;    

        var xDiff = xDown - xUp;
        var yDiff = yDown - yUp;

        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
            if ( xDiff > 0 ) {
                /* left swipe */
                console.log("left swipe") 
            } else {
                /* right swipe */
                console.log("right swipe")
            }                       
        } else {
            if ( yDiff > 0 ) {
                /* up swipe */ 
                console.log("up swipe")
            } else { 
                /* down swipe */
                console.log("down swipe")
            }                                                                 
        }
    /* reset values */
        xDown = null;
        yDown = null;                                             
    };
})();

(function(){
    wheelUpArea = document.getElementById('mouse_wheel_up');
    wheelUpArea.addEventListener('touchstart', handleTouchStart, false);        
    //wheelUpArea.addEventListener('touchmove', handleTouchMove, false);
    wheelUpArea.addEventListener('touchend', handleTouchEnd, false);

    var timer;

    function handleTouchStart(evt) {  

        $.get("mouse_WheelRotateUp");
        timer = setInterval(function(){
            $.get("mouse_WheelRotateUp");
        }, 300);

    };

    function handleTouchEnd(evt) {
        clearInterval(timer);
    }

})();

(function(){
    wheelUpArea = document.getElementById('mouse_wheel_down');
    wheelUpArea.addEventListener('touchstart', handleTouchStart, false);            
    wheelUpArea.addEventListener('touchend', handleTouchEnd, false);

    var timer;

    function handleTouchStart(evt) {  

        $.get("mouse_WheelRotateDown");
        timer = setInterval(function(){
            $.get("mouse_WheelRotateDown");
        }, 300);

    };

    function handleTouchEnd(evt) {
        clearInterval(timer);
    }

})();