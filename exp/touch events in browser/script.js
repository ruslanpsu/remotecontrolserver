(function(){
    touchArea = document.getElementById('touch_area');
    touchArea.addEventListener('touchstart', handleTouchStart, false);        
    touchArea.addEventListener('touchmove', handleTouchMove, false);
    touchArea.addEventListener('touchend', handleTouchEnd, false);

    var xDown = null;                                                        
    var yDown = null;                                                        
    var count = 0;    
    var startPoint = {};
    var prevPoint = {x:0, y:0};
    var rt = 30;
    var drSum = 0;


    function handleTouchStart(evt) {                                         
        xDown = evt.touches[0].clientX;                                      
        yDown = evt.touches[0].clientY;   

        startPoint.x = xDown;
        startPoint.y = yDown;

        console.log("tapped at: ("+evt.touches[0].clientX+"," + evt.touches[0].clientY +")");                                   
    };                                                

    function getDistance(p1, p2){
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    function PointDifference(p1, p2){
        var result = {};
        result.x = p1.x - p2.x;
        result.y = p1.y - p2.y;
        return result;
    }

    function handleTouchEnd(evt){
        count = 0;
    }

    function handleTouchMove(evt) {
        var ms = new Date().getTime();

        var point = {};
        point.x = evt.touches[0].clientX;
        point.y = evt.touches[0].clientY;

        var d = getDistance(startPoint, point) ;

        var difPoint = PointDifference(point, prevPoint);
        var dr = getDistance(point, prevPoint);

        drSum += dr;
        if (drSum > rt){
            console.log(++count + ". (" + point.x+"," + point.y +") - time: " + ms + " d=" + d + " dx=" +difPoint.x + " dy=" + difPoint.y + " dr=" + dr + " drSum=" + drSum);
            drSum = 0;
        };
        

        prevPoint = point;

        if ( ! xDown || ! yDown ) {
            return;
        }

        var xUp = evt.touches[0].clientX;                                    
        var yUp = evt.touches[0].clientY;    

        var xDiff = xDown - xUp;
        var yDiff = yDown - yUp;

        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
            if ( xDiff > 0 ) {
                /* left swipe */
                console.log("left swipe") 
            } else {
                /* right swipe */
                console.log("right swipe")
            }                       
        } else {
            if ( yDiff > 0 ) {
                /* up swipe */ 
                console.log("up swipe")
            } else { 
                /* down swipe */
                console.log("down swipe")
            }                                                                 
        }
    /* reset values */
        xDown = null;
        yDown = null;                                             
    };
})();